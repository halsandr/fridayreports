from spreadsheetSplit import SpreadsheetSplit
import yaml
import os
import shutil
from emailer import Emailer


class Main:
    def __init__(self, config_file_name):
        self.config_file_name = config_file_name
        self.config = {}

    def load_config(self):
        with open(self.config_file_name) as config:
            self.config = yaml.load(config, Loader=yaml.FullLoader)

    def clean_output_folder(self):
        for filename in os.listdir(self.config["output_folder"]):
            file_path = os.path.join(self.config["output_folder"], filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))


if __name__ == "__main__":
    print("Loading config.yaml\r\n")
    m = Main("config.yaml")
    m.load_config()

    print("Cleaning output\r\n")
    if m.config["clean_output_folder"]:
        m.clean_output_folder()

    print("Reading input spreadsheet\r\n")
    my_splitter = SpreadsheetSplit(m.config["input_file"])

    my_splitter.output_folder = m.config["output_folder"]
    my_splitter.worksheet_name = m.config["worksheet_name"]
    my_splitter.client_name_field = m.config["client_name_field"]

    my_splitter.build_client_list()
    my_splitter.print_client_names()

    print("\r\nPlease check that these values look correct\r\n")
    input("Hit enter to continue...\r\n")

    my_splitter.split_spreadsheet()

    print(f"\r\nSpreadsheets exported to: {m.config['output_folder']}\r\n")
    input("Hit enter to continue...\r\n")

    print("\r\nGenerating Emails\r\n")
    my_emailer = Emailer()
    my_emailer.send_subject = m.config["email_subject"]
    my_emailer.send_body = m.config["email_body"]
    my_emailer.from_address = m.config["from_email"]

    for client in my_splitter.client_names:
        if client in m.config['clients']:
            my_emailer.send_addresses[m.config['clients'][client]] = f"{m.config['output_folder']}/{client}.xlsx"

    my_emailer.loop_addresses()
    print("Done!")
    input("Hit enter to finish...")
