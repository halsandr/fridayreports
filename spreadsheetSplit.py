from openpyxl import load_workbook, Workbook


class SpreadsheetSplit:

    def __init__(self, input_file):

        self.input_file = input_file
        self.output_folder = "output"
        self.worksheet_name = "Sheet1"
        self.client_name_field = "Company/individual"

        self.client_names = {}
        self.field_names = []
        self.client_name_column = ""

        try:
            self.wb = load_workbook(filename=input_file)
            self.ws = self.wb[self.worksheet_name]
        except FileNotFoundError:
            print(f"could not load: {self.input_file}")
            input("Hit enter to finish...")
            exit()

    def build_client_list(self):
        for name in self.ws['1:1']:
            self.field_names.append(name.value)
            if name.value == self.client_name_field:
                self.client_name_column = name.column_letter

        try:
            for client in self.ws[f"{self.client_name_column}:{self.client_name_column}"]:
                if client.value != self.client_name_field:
                    if client.value in self.client_names:
                        self.client_names[client.value] = self.client_names[client.value] + 1
                    else:
                        self.client_names[client.value] = 1
        except ValueError:
            print(f"Could not find column: {self.client_name_field}")
            input("Hit enter to finish...")
            exit()

    def print_client_names(self):
        for client in self.client_names:
            print(f"{client}: {self.client_names[client]}")

    def split_spreadsheet(self):
        for client in self.client_names:
            new_wb = Workbook()
            destination_filename = f"{self.output_folder}/{client}.xlsx"

            new_ws = new_wb.active
            new_ws.append(self.field_names)
            for row in self.ws:
                row_values = []
                pass_row = True

                for cell in row:
                    if cell.column_letter == self.client_name_column:
                        if cell.value == client:
                            pass_row = False

                if not pass_row:
                    for cell in row:
                        row_values.append(cell.value)
                    new_ws.append(row_values)

            new_wb.save(filename=destination_filename)


if __name__ == "__main__":
    my_splitter = SpreadsheetSplit("Examples/example.xlsx")

    my_splitter.worksheet_name = "Sheet1"
    my_splitter.client_name_field = "Company/Individual"

    my_splitter.build_client_list()
    my_splitter.print_client_names()
    input("\r\nHit enter to continue...\r\n")

    my_splitter.split_spreadsheet()
