import win32com.client as win32
from pathlib import Path, PureWindowsPath


class Emailer:

    def __init__(self):
        self.outlook = win32.Dispatch('outlook.application')
        self.from_address = ""
        self.send_addresses = {}
        self.send_subject = ""
        self.send_body = ""

    def create_email(self, to_address, attachment_path=None):
        mail = self.outlook.CreateItem(0)
        mail.To = to_address

        if self.send_subject:
            mail.Subject = self.send_subject
        if self.send_body:
            mail.HtmlBody = self.send_body
        if attachment_path:
            mail.Attachments.Add(str(Path(attachment_path).absolute()))
        if self.from_address:
            mail.SentOnBehalfOfName = self.from_address

        mail.Display(True)

    def loop_addresses(self):
        for address in self.send_addresses:
            self.create_email(address, self.send_addresses[address])


if __name__ == "__main__":
    my_emailer = Emailer()

    my_emailer.send_addresses["test@test.co.uk"] = "Examples/example.xlsx"
    my_emailer.send_addresses["test@test.com"] = "Examples/example.xlsx"

    my_emailer.send_subject = "test subject"
    my_emailer.send_body = "test body"

    my_emailer.loop_addresses()
    # my_emailer.create_email("test@test.co.uk", "Examples\\example.xlsx")
