from cx_Freeze import setup, Executable

base = None

executables = [
    Executable("main.py", base=base),
]
include_files = ['output', 'Examples', 'input', 'config.yaml']

options = {
    'build_exe': {
        'include_files': include_files,
        'include_msvcr': True,
        "excludes": ["tkinter", "multiprocessing"],
        'optimize': 2
    },
}

setup(
    name="fridayReports",
    options=options,
    version="0.1",
    description='Tool for generating friday reports',
    executables=executables
)
